package AngajatiApp.repository;

import AngajatiApp.model.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeMockTest {

    private EmployeeMock em;

    @Before
    public void setUp() throws Exception {
        em = new EmployeeMock();
        System.out.println("Before test");
    }

    @After
    public void tearDown() throws Exception {
        em = null;
        System.out.println("After test");
    }

    @Test
    public void TC1() {
        Employee e1 = new Employee();

        e1.setId(1);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(32.5);
        assertEquals(true, em.addEmployee(e1));
    }

    @Test
    public void TC2() {
        Employee e1 = new Employee();

        e1.setId(1); // ?
        e1.setLastName("Daniel");
        e1.setFirstName("");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(32.5);
        assertEquals(false, em.addEmployee(e1));
    }

    @Test
    public void TC3() {
        Employee e1 = new Employee();

        e1.setId(1);
        e1.setLastName("Daniel");
        e1.setFirstName("Io");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(32222.5);
        assertEquals(false, em.addEmployee(e1));
    }

    @Test
    public void TC4() throws EmployeeException {
        Employee e1 = new Employee();

        /*e1.setId(1);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.SECRETARIAT);
        e1.setSalary(32.5);*/

        e1.getEmployeeFromString("Daniel;Ionut;1234567891111;SECRETARIAT;32.5;0", 11);
        assertEquals(false, em.addEmployee(e1));

    }


    @Test
    public void modifyEmp1() {   //TC1
        Employee e1 = null;
        List<Employee> employeeMockList = em.getEmployeeList();
        em.modifyEmployeeFunction(e1, DidacticFunction.ASISTENT);
        assertTrue(em.getEmployeeList().equals(employeeMockList));
    }

    @Test
    public void modifyEmp2() {   //TC2

        Employee e1 = new Employee();
        e1.setId(1);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(32.5);
        em.addEmployee(e1);

        List<Employee> employeeMockList = em.getEmployeeList();
        em.modifyEmployeeFunction(e1, DidacticFunction.LECTURER);
        assertTrue(e1.getFunction() == DidacticFunction.LECTURER);
    }


    @Test
    public void modifyEmp3() {   //TC3

        List<Employee> employeeMockList = em.getEmployeeList();

        Employee e1 = new Employee();
        e1.setId(100);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(32.5);
        em.addEmployee(e1);

        em.modifyEmployeeFunction(e1, DidacticFunction.ASISTENT);
        assertTrue(em.getEmployeeList().equals(employeeMockList));


        // em.modifyEmployeeFunction(e1,DidacticFunction.LECTURER);

//
//        List<Employee> t = em.getEmployeeList();
//        em.addEmployee(e1);
//        em.modifyEmployeeFunction(e1, DidacticFunction.LECTURER);
//
//        for (Employee e : t)
//            if (e.getId() == e1.getId())
//                assertEquals(false, e.getFunction() == DidacticFunction.ASISTENT);
//
//        List<Employee> employeeMockList = em.getEmployeeList();
//        em.modifyEmployeeFunction(e1, DidacticFunction.ASISTENT);
//        assertTrue(em.getEmployeeList().equals(employeeMockList));
//
//
    }




}
