package AngajatiApp.model;

import org.junit.*;

import static org.junit.Assert.*;

public class EmployeeTest {

    private Employee e1;
    private Employee e2;
    private Employee e3;
    private Employee e4;

    //se executa automat inaintea fiecarei metode de test
    @Before
    public void setUp() {
        e1 = new Employee();//
        e1.setFirstName("Daniel");
        e2 = new Employee();
        e2.setFirstName("Ionut");
        e3 = new Employee();
        e4 = null;
        System.out.println("Before test");
    }


    //se executa automat dupa fiecarei metoda de test
    @After
    public void tearDown() {
        e1 = null;
        e2 = null;
        e3 = null;
        System.out.println("After test");
    }

    @Ignore
    @Test
    public void testGetFirstName() {
        assertEquals("Daniel", e1.getFirstName());
    }

    @Test (expected=NullPointerException.class)
    public void testGetTitlu2() {
        assertEquals("Daniel", e4.getFirstName());
    }


    @Test (timeout=100) //asteapta 10 milisecunde
        public void testFictiv() {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Test
        public void testConstructor(){
            assertNotEquals("verificam daca s-a creat employee 1",e1,null);
        }
        @BeforeClass
        public static void setUpAll(){
            System.out.println("Before All tests - at the beginning of the Test Class");
        }
        @AfterClass
        public static void tearDownAll() {
            System.out.println("After All tests - at the end of the Test Class");
        }

        @Test
        public void testSetFirstName() {
        e1.setFirstName("Vlad");
            assertEquals("Vlad",e1.getFirstName());
        }

        @Test (expected=NullPointerException.class)
        public void testSetFirstName2() {
            assertEquals("Ionut", e4.getFirstName());
        }


}